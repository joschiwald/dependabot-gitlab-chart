#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

yaml="$CHART_DIR/ci/ci-values.yaml"

log "Update chart dependencies"
helm dependency update $CHART_DIR

log "Validate helm template files"
helm template $CHART_DIR -f $yaml | kubeval --strict --additional-schema-locations https://raw.githubusercontent.com/joshuaspence/kubernetes-json-schema/master
