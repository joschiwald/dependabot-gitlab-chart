#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log "Linting README.md"
helm-docs -c charts -o ../../README.md
git diff --exit-code
